//
//  PaymentRepository.swift
//  ML-Demo
//
//  Created by Rodrigo  on 23/07/18.
//  Copyright © 2018 Rodrigo . All rights reserved.
//

import Foundation
import Unbox

class APIRepoitory {
    static func getPayments(completionHandler: @escaping ([Payment]) -> Swift.Void) {
        let PATH = "payment_methods?public_key=\(ApiDefs.PUBLIC_KEY)"
        ConnectionManager.request(url: "\(ApiDefs.API_URL)\(PATH)", method: .get) { (statusCode, response) in
            var payments = [Payment]()
                do {
                    let responseArray = response as? NSArray ?? NSArray()
                    for element in responseArray {
                        var payment = Payment()
                        let dict = element as? NSDictionary ?? NSDictionary()
                        payment = try unbox(dictionary: dict as! UnboxableDictionary)
                        payments.append(payment)
                    }
                }catch {
                    print("An error occurred: \(error)")
                }
            completionHandler(payments)
        }
    }
    
    static func getBanks(id: String, completionHandler: @escaping ([Bank]) -> Swift.Void) {
        let PATH = "payment_methods/card_issuers?public_key=\(ApiDefs.PUBLIC_KEY)&payment_method_id=\(id)"
        ConnectionManager.request(url: "\(ApiDefs.API_URL)\(PATH)", method: .get) { (statusCode, response) in
            var banks = [Bank]()
            do {
                let responseArray = response as? NSArray ?? NSArray()
                for element in responseArray {
                    var bank = Bank()
                    let dict = element as? NSDictionary ?? NSDictionary()
                    bank = try unbox(dictionary: dict as! UnboxableDictionary)
                    banks.append(bank)
                }
            }catch {
                print("An error occurred: \(error)")
            }
            completionHandler(banks)
        }
    }
    
    static func getInstallments(amoung: String, paymentMethodId: String, issuerId: String, completionHandler: @escaping ([Installment]) -> Swift.Void) {
        let PATH = "payment_methods/installments?public_key=\(ApiDefs.PUBLIC_KEY)&payment_method_id=\(paymentMethodId)&amount=\(amoung)&issuer.id=\(issuerId)"
        ConnectionManager.request(url: "\(ApiDefs.API_URL)\(PATH)", method: .get) { (statusCode, response) in
            var installments = [Installment]()
            do {
               let responseArray = response as? NSArray ?? NSArray()
                let dict = responseArray[0] as? NSDictionary ?? NSDictionary()
                let payerArray = dict["payer_costs"] as? NSArray ?? NSArray()
                for element in payerArray {
                    var installment = Installment()
                    let dict = element as? NSDictionary ?? NSDictionary()
                    installment = try unbox(dictionary: dict as! UnboxableDictionary)
                    installments.append(installment)
                }
            }catch {
                print("An error occurred: \(error)")
            }
            completionHandler(installments)
        }
    }
    
}

//
//  ApiDefs.swift
//  ML-Demo
//
//  Created by Rodrigo  on 23/07/18.
//  Copyright © 2018 Rodrigo . All rights reserved.
//

import Foundation

struct ApiDefs {
    static let API_URL = "https://api.mercadopago.com/v1/"
    static let PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88"
}

//
//  ConnectionManager.swift
//  ML-Demo
//
//  Created by Rodrigo  on 23/07/18.
//  Copyright © 2018 Rodrigo . All rights reserved.
//

import Foundation

import Alamofire

class ConnectionManager {
    
    static func request(url: URLConvertible, method: HTTPMethod, completionHandler: @escaping (Int?, Any?) -> Swift.Void ) {
        Alamofire.request(url , method: method, parameters:nil , encoding: JSONEncoding.default, headers:nil).responseJSON { (response) in
            var statusCode = Int()
            
            if let serverStatusCode = response.response?.statusCode {
                statusCode = serverStatusCode
            } else {
                statusCode = -1
            }
            
            let error = response.result.isFailure
            if error {
                print(error)
            }
            
            if let error = response.result.error as? AFError {
                print(error)
            }
            print(response)
            var jsonReturn : Any = ()
            if let JSON = response.result.value {
                jsonReturn = JSON
            }
            completionHandler(statusCode, jsonReturn)
        }
    }
}

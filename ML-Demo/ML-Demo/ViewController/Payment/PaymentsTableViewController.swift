//
//  PaymentsTableViewController.swift
//  ML-Demo
//
//  Created by Rodrigo  on 23/07/18.
//  Copyright © 2018 Rodrigo . All rights reserved.
//

import UIKit
import Kingfisher

class PaymentsTableViewController: UITableViewController {
    var value = String()
    var payments: [Payment] = [Payment]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Payments"
        self.getPayments()
    }
    
    func getPayments() {
        APIRepoitory.getPayments { (payments) in
            DispatchQueue.main.async {
                self.payments = payments
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return payments.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentsTableViewCell", for: indexPath) as! PaymentsTableViewCell
        let payment = self.payments[indexPath.row]
        cell.nameLabel.text = payment.name
        cell.cardImage.kf.setImage(with: URL(string: payment.thumbnail!))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let payment = self.payments[indexPath.row]
        let controller = storyboard?.instantiateViewController(withIdentifier: "BanksTableViewController") as? BanksTableViewController
        controller?.payment = payment
        controller?.value = value
        self.navigationController?.pushViewController(controller!, animated: true)
    }
}

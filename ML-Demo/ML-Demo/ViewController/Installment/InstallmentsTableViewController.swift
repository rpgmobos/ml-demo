//
//  InstallmentsTableViewController.swift
//  ML-Demo
//
//  Created by Rodrigo  on 23/07/18.
//  Copyright © 2018 Rodrigo . All rights reserved.
//

import UIKit

class InstallmentsTableViewController: UITableViewController {
    var payment = Payment()
    var bank = Bank()
    var value = String()
    var installments = [Installment]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Installments"
        self.loadInstallments()
    }
    
    func loadInstallments() {
        APIRepoitory.getInstallments(amoung: value, paymentMethodId: payment.id!, issuerId: bank.id!) { (installments) in
            DispatchQueue.main.async {
                self.installments = installments
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return installments.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IntallmentsTableViewCell", for: indexPath) as! IntallmentsTableViewCell
        let installment = self.installments[indexPath.row]
        cell.messageLabel.text = installment.recommended_message!
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let installment = self.installments[indexPath.row]
        let message = "\(value) em \(String(describing: installment.installments!)) vezes no \(String(describing: payment.name!))"
        UserDefaults.standard.setValue(message, forKey: "Message")
        NotificationCenter.default.post(name: .postNotifi, object: nil, userInfo: nil)
        self.dismiss(animated: true, completion: nil)
    }
}

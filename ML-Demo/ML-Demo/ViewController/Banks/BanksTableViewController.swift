//
//  BanksTableViewController.swift
//  ML-Demo
//
//  Created by Rodrigo  on 23/07/18.
//  Copyright © 2018 Rodrigo . All rights reserved.
//

import UIKit

class BanksTableViewController: UITableViewController {
    var payment = Payment()
    var banks = [Bank]()
    var value = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Banks"
        self.getBanks()
    }
    
    func getBanks() {
        APIRepoitory.getBanks(id: payment.id!) { (banks) in
            DispatchQueue.main.async {
                self.banks = banks
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return banks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BanksTableViewCell", for: indexPath) as! BanksTableViewCell
        let bank = self.banks[indexPath.row]
        cell.nameLabel.text = bank.name
        cell.cardImage.kf.setImage(with: URL(string: bank.thumbnail!))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bank = self.banks[indexPath.row]
        let controller = storyboard?.instantiateViewController(withIdentifier: "InstallmentsTableViewController") as? InstallmentsTableViewController
        controller?.payment = payment
        controller?.bank = bank
        controller?.value = value
        self.navigationController?.pushViewController(controller!, animated: true)
    }

}

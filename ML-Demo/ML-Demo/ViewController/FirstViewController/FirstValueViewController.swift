//
//  FirstValueViewController.swift
//  ML-Demo
//
//  Created by Rodrigo  on 23/07/18.
//  Copyright © 2018 Rodrigo . All rights reserved.
//

import UIKit

extension Notification.Name {
    static let postNotifi = Notification.Name("postNotifi")
}

class FirstValueViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var valueTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Value"
        // Do any additional setup after loading the view.
    }

    @objc func showAlert(notfication: NSNotification) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: UserDefaults.standard.string(forKey: "Message"), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        NotificationCenter.default.removeObserver(self, name: .postNotifi, object: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func next(_ sender: Any) {
        NotificationCenter.default.addObserver(self, selector: #selector(showAlert(notfication:)), name: .postNotifi, object: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "PaymentNavigationController") as? UINavigationController
        let controller: PaymentsTableViewController = nav!.viewControllers.first as! PaymentsTableViewController
        controller.value = valueTextField.text!
        self.present(nav!, animated: true, completion: nil)
    }
    
}

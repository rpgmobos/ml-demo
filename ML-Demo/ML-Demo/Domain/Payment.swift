//
//  Payment.swift
//  ML-Demo
//
//  Created by Rodrigo  on 23/07/18.
//  Copyright © 2018 Rodrigo . All rights reserved.
//

import Foundation
import Unbox

struct Payment: Unboxable {
    let id: String?
    let name: String?
    let thumbnail: String?
    
    init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "id")
        self.name = unboxer.unbox(key: "name")
        self.thumbnail = unboxer.unbox(key: "thumbnail")
    }
    
    init() {
        self.id = ""
        self.name = ""
        self.thumbnail = ""
    }
}

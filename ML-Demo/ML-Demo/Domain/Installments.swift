//
//  Installments.swift
//  ML-Demo
//
//  Created by Rodrigo  on 23/07/18.
//  Copyright © 2018 Rodrigo . All rights reserved.
//

import Foundation

import Unbox

struct Installment: Unboxable {
    let installments: Int?
    let recommended_message: String?
    
    
    init(unboxer: Unboxer) throws {
        self.installments = unboxer.unbox(key: "installments")
        self.recommended_message = unboxer.unbox(key: "recommended_message")
    }
    
    init() {
        self.installments = 0
        self.recommended_message = ""
    }
}
